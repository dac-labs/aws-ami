#!/bin/bash

# Set the AMI name and region
ami_name="${AMI_NAME}"
region="ap-southeast-2"

# Use the AWS CLI to describe images with the specified name
output=$(aws ec2 describe-images --filters "Name=name,Values=$ami_name" --region $region)

# Check if the output contains any images
if [[ $output == *"ImageId"* ]]; then
  echo "AMI with name $ami_name exists in region $region"
  exit 1
else
  exit 0
fi