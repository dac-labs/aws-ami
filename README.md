# AWS AMI Deployment

This project showcases example on utilizing Hashicorp's Packer on provisioning AWS AMI as a template baseline for application deployments.

The project was project challenge from a DevOps community named DevOps and Cloud Labs, anyone can join us at https://www.youtube.com/@DevOps-Cloud. We are exited and we will wait for you there.

## Documentation

Variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY must be declared in your project or project-group as this is used to access the AWS account where the AMI will be deployed.

## Acknowledgements

- Thank you to the Admins of the group at [DevOps and Cloud Labs](https://awesomeopensource.com/project/elangosundar/awesome-README-templates) Channel on giving us beginner friendly projects that allows us to push the limits on what we can do despite lacking expience.
